package com.example.mytp2android;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.content.Intent;
import android.database.Cursor;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class BookActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        Intent intent = getIntent();
        final Book book = (Book) intent.getExtras().get("book");
        final long id = book.getId();

        TextView textViewName = (TextView) findViewById(R.id.nameBook) ;
        textViewName.setText(book.getTitle());

        TextView textViewAuthor = (TextView) findViewById(R.id.editAuthors) ;
        textViewAuthor.setText(book.getAuthors());

        TextView textViewYears = (TextView) findViewById(R.id.editYear) ;
        textViewYears.setText(book.getYear());

        TextView textViewGenres = (TextView) findViewById(R.id.editGenres) ;
        textViewGenres.setText(book.getGenres());

        TextView textViewPublisher = (TextView) findViewById(R.id.editPublisher) ;
        textViewPublisher.setText(book.getPublisher());


        Button sauvegarder = (Button) findViewById(R.id.button);

        sauvegarder.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view)
            {

                EditText editName = (EditText) findViewById(R.id.nameBook);
                EditText editAuthors = (EditText) findViewById(R.id.editAuthors);
                EditText editYear = (EditText) findViewById(R.id.editYear);
                EditText editGenre =(EditText) findViewById(R.id.editGenres);
                EditText editPublisher = (EditText) findViewById(R.id.editPublisher);


                String name = editName.getText().toString();
                String author = editAuthors.getText().toString();
                String year = editYear.getText().toString();
                String genre = editGenre.getText().toString();
                String publisher = editPublisher.getText().toString();


                if(!name.isEmpty() && !author.isEmpty() && !year.isEmpty() && !genre.isEmpty() && !publisher.isEmpty())
                {
                    Book book = new Book(id, name, author, year, genre, publisher);
                    BookDbHelper bookDbHelper = new BookDbHelper(BookActivity.this);
                    if(id != 0) {
                        bookDbHelper.updateBook(book);
                    }
                    else
                        bookDbHelper.addBook(book);

                    Toast.makeText(BookActivity.this, "Sauvegarde effectué",
                            Toast.LENGTH_LONG).show();


                    Intent it = new Intent(BookActivity.this,MainActivity.class) ;
                    startActivity(it);
                }

                else
                {
                    Toast.makeText(BookActivity.this, "Un champs est vide",
                            Toast.LENGTH_LONG).show();
                }
            } ;
        }) ;
    }
}
