package com.example.mytp2android;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class BookDbHelper extends SQLiteOpenHelper
{
    private static final String TAG = BookDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "book.db";

    public static final String TABLE_NAME = "library";

    public static final String _ID = "_id";
    public static final String COLUMN_BOOK_TITLE = "title";
    public static final String COLUMN_AUTHORS = "authors";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_GENRES = "genres";
    public static final String COLUMN_PUBLISHER = "publisher";


    private String CREATE_TABLES = "CREATE TABLE " + TABLE_NAME + " ("
            + _ID + " INTEGER PRIMARY KEY, "
            + COLUMN_BOOK_TITLE + " VARCHAR, "
            + COLUMN_AUTHORS + " VARCHAR, "
            + COLUMN_YEAR + " VARCHAR, "
            + COLUMN_GENRES + " VARCHAR, "
            + COLUMN_PUBLISHER + " VARCHAR )";


    public BookDbHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_TABLES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }


    /**
     * Adds a new book
     * @return  true if the book was added to the table ; false otherwise
     */
    public boolean addBook(Book book)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        long rowID = 0;

        ContentValues cv= new ContentValues();
        cv.put(COLUMN_BOOK_TITLE,book.getTitle());
        cv.put(COLUMN_AUTHORS,book.getAuthors());
        cv.put(COLUMN_YEAR,book.getYear());
        cv.put(COLUMN_GENRES,book.getGenres());
        cv.put(COLUMN_PUBLISHER,book.getPublisher());
        rowID = db.insert(TABLE_NAME, null, cv);
        db.close();

        return (rowID != -1);
    }

    /**
     * Updates the information of a book inside the data base
     * @return the number of updated rows
     */

    public int updateBook(Book book)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        int res;
        // updating row
        // call db.update()

        ContentValues cv = new ContentValues();
        cv.put(COLUMN_BOOK_TITLE, book.getTitle());
        cv.put(COLUMN_AUTHORS, book.getAuthors());
        cv.put(COLUMN_YEAR, book.getYear());
        cv.put(COLUMN_GENRES, book.getGenres());
        cv.put(COLUMN_PUBLISHER, book.getPublisher());

        res = db.update(TABLE_NAME, cv, "_id="+book.getId(), null);
        return res;
    }


    public Cursor fetchAllBooks()
    {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor;

        cursor = db.rawQuery("select * from " + TABLE_NAME, null);

        if (cursor != null)
        {
            cursor.moveToFirst();
        }
        return cursor;
    }

  /*  public void deleteCity(Cursor cursor)
    {
        SQLiteDatabase db = this.getWritableDatabase();
        // call db.delete();
        db.close();
    }*/



    public static Book cursorToBook(Cursor cursor)
    {

        int idIndex = cursor.getColumnIndexOrThrow("_id");
        int nameTitle = cursor.getColumnIndexOrThrow("title");
        int nameAuthors = cursor.getColumnIndexOrThrow("authors");
        int nameYears = cursor.getColumnIndexOrThrow("year");
        int nameGenres = cursor.getColumnIndexOrThrow("genres");
        int namePublisher = cursor.getColumnIndexOrThrow("publisher");


        long id = cursor.getLong(idIndex);
        String name = cursor.getString(nameTitle);
        String author = cursor.getString(nameAuthors) ;
        String years = cursor.getString(nameYears) ;
        String genres = cursor.getString(nameGenres) ;
        String publi = cursor.getString(namePublisher) ;


        Book book = new Book(id, name, author, years, genres, publi) ;
        return book;
    }


    public void delete()
    {
        SQLiteDatabase db = this.getWritableDatabase();
        String a = "Delete from "+ TABLE_NAME;
        db.execSQL(a);

    }


    public void populate()
    {
        Log.d(TAG, "call populate()");
        addBook(new Book("Rouge Brésil", "J.-C. Rufin", "2003", "roman d'aventure, roman historique", "Gallimard"));
        addBook(new Book("Guerre et paix", "L. Tolstoï", "1865-1869", "roman historique", "Gallimard"));
        addBook(new Book("Fondation", "I. Asimov", "1957", "roman de science-fiction", "Hachette"));
        addBook(new Book("Du côté de chez Swan", "M. Proust", "1913", "roman", "Gallimard"));
        //addBook(new Book("Le Comte de Monte-Cristo", "A. Dumas", "1844-1846", "roman-feuilleton", "Flammarion"));
        //addBook(new Book("L'Iliade", "Homère", "8e siècle av. J.-C.", "roman classique", "L'École des loisirs"));
        //addBook(new Book("Histoire de Babar, le petit éléphant", "J. de Brunhoff", "1931", "livre pour enfant", "Éditions du Jardin des modes"));
        //addBook(new Book("Le Grand Pouvoir du Chninkel", "J. Van Hamme et G. Rosiński", "1988", "BD fantasy", "Casterman"));
        //addBook(new Book("Astérix chez les Bretons", "R. Goscinny et A. Uderzo", "1967", "BD aventure", "Hachette"));
        //addBook(new Book("Monster", "N. Urasawa", "1994-2001", "manga policier", "Kana Eds"));
        //addBook(new Book("V pour Vendetta", "A. Moore et D. Lloyd", "1982-1990", "comics", "Hachette"));

        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }

}
