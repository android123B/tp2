package com.example.mytp2android;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.database.Cursor;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.content.Intent;
import android.widget.AdapterView;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BookDbHelper bookDbHelper = new BookDbHelper(this);

        //bookDbHelper.delete();
        bookDbHelper.populate();

        Cursor cursor = bookDbHelper.fetchAllBooks();

        String[] coloms = new String[] {
                BookDbHelper.COLUMN_BOOK_TITLE, BookDbHelper.COLUMN_AUTHORS
        };

        ListView listView = (ListView) findViewById(R.id.adapter);


        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_list_item_2,
                cursor,
                coloms,
                new int[] {android.R.id.text1, android.R.id.text2});

        listView.setAdapter(adapter);



        //affiche info livre
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id)
            {
                // Do something in response to the click
                Intent intent = new Intent(MainActivity.this, BookActivity.class);
                Cursor cursor = (Cursor) parent.getItemAtPosition(position);
                Book book = BookDbHelper.cursorToBook(cursor);
                intent.putExtra("book", book);
                startActivity(intent);
            }
        });


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //ajoute un livre
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent it = new Intent(MainActivity.this, BookActivity.class);
                Book book = new Book(0,"", "", "", "", "");
                it.putExtra("book", book);
                startActivity(it);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
